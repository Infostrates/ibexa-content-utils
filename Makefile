test: phpstan phpunit phpcs

phpstan:
	php -d memory_limit=-1 vendor/bin/phpstan

phpunit:
	php vendor/bin/phpunit

phpcs:
	php vendor/bin/phpcs

phpcbf:
	php vendor/bin/phpcbf
