<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentUtils;

use DomainException;
use Ibexa\Contracts\Core\Repository\LocationService;
use Ibexa\Contracts\Core\Repository\Exceptions\NotFoundException as ApiNotFoundException;
use Ibexa\Contracts\Core\Repository\Exceptions\UnauthorizedException as ApiUnauthorizedException;
use Ibexa\Contracts\Core\Repository\LanguageResolver;
use Ibexa\Contracts\Core\Repository\Values\Content\Location;
use Ibexa\Contracts\Core\SiteAccess\ConfigResolverInterface;

class LocationServiceHelper
{
    private LocationService $locationService;
    private LanguageResolver $languageResolver;
    private ConfigResolverInterface $configResolver;

    public function __construct(
        LocationService $locationService,
        LanguageResolver $languageResolver,
        ConfigResolverInterface $configResolver
    ) {
        $this->locationService = $locationService;
        $this->languageResolver = $languageResolver;
        $this->configResolver = $configResolver;
    }

    /**
     * @param mixed $locationId
     * @return Location
     */
    public function loadLocation($locationId, bool $applyVisibility = false): ?Location
    {
        try {
            $location = $this->locationService->loadLocation(
                $locationId,
                $this->languageResolver->getPrioritizedLanguages()
            );

            return ($applyVisibility && $location->hidden) ? null : $location;
        } catch (ApiNotFoundException | ApiUnauthorizedException $e) {
            return null;
        }
    }

    public function loadLocationByRemoteId(string $remoteId, bool $applyVisibility = false): ?Location
    {
        try {
            $location = $this->locationService->loadLocationByRemoteId(
                $remoteId,
                $this->languageResolver->getPrioritizedLanguages()
            );

            return ($applyVisibility && $location->hidden) ? null : $location;
        } catch (ApiNotFoundException | ApiUnauthorizedException $e) {
            return null;
        }
    }

    /**
     * @param Location[] $locationList
     * @return int[]
     */
    public function getLocationIdsFromLocationList(array $locationList): array
    {
        return array_map(static function (Location $location) {
            return $location->id;
        }, $locationList);
    }

    public function getRootLocationId(): int
    {
        return (int)$this->configResolver->getParameter('content.tree_root.location_id');
    }

    public function loadRootLocation(): Location
    {
        try {
            return $this->locationService->loadLocation($this->getRootLocationId());
        } catch (ApiNotFoundException | ApiUnauthorizedException $e) {
            throw new DomainException('Unable to load root location');
        }
    }
}
