<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentUtils\Image\TwigExtension;

final class ResponsiveImage
{
    /** @var string[] */
    private array $variationNameList;
    /** @var mixed */
    private mixed $sizesValue;

    /**
     * @param string[] $variationNameList
     */
    public function __construct(array $variationNameList, mixed $sizesValue)
    {
        $this->variationNameList = $variationNameList;
        $this->sizesValue = $sizesValue;
    }

    /**
     * @return string[]
     */
    public function getVariationNameList(): array
    {
        return $this->variationNameList;
    }

    public function getSizesValue(): mixed
    {
        return $this->sizesValue;
    }
}
