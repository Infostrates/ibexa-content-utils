<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentUtils\Image\TwigExtension;

use DomainException;
use Ibexa\Contracts\Core\Repository\Values\Content\Content;
use Ibexa\Contracts\Core\Repository\Values\Content\Field;
use Ibexa\Core\FieldType\Image\Value as ImageValue;
use Ibexa\Core\FieldType\ImageAsset\Value as ImageAssetValue;
use Ibexa\Core\Helper\FieldHelper;
use Ibexa\Core\Helper\TranslationHelper;
use Ibexa\Contracts\Core\Variation\Values\ImageVariation;
use Ibexa\Contracts\Core\Variation\Values\Variation;
use Ibexa\Contracts\Core\Variation\VariationHandler;
use Infostrates\IbexaContentUtils\Image\TwigExtension\ResponsiveImage;
use RuntimeException;
use Symfony\Component\Asset\PackageInterface;
use Twig\Environment;
use Twig\Error\RuntimeError;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class EzRenderImgTagExtension extends AbstractExtension
{
    private VariationHandler $variationHandler;
    private TranslationHelper $translationHelper;
    private PackageInterface $package;
    private Environment $environment;
    private FieldHelper $fieldHelper;

    public function __construct(
        VariationHandler $variationHandler,
        TranslationHelper $translationHelper,
        PackageInterface $package,
        Environment $environment,
        FieldHelper $fieldHelper
    ) {
        $this->variationHandler = $variationHandler;
        $this->translationHelper = $translationHelper;
        $this->package = $package;
        $this->environment = $environment;
        $this->fieldHelper = $fieldHelper;
    }


    /**
     * @return TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('ez_render_img_tag', [$this, 'ezRenderImgTag'], ['is_safe' => ['html']]),
            new TwigFunction('ez_src', [$this, 'getSrc']),
            new TwigFunction('responsive_image', [$this, 'getResponsiveImage']),
        ];
    }

    /**
     * @param Content $content
     * @param string $fieldIdentifier
     * @param string|ResponsiveImage $variationNameOrResponsiveImage
     * @param array<string, mixed> $htmlAttributes
     * @param bool $addSizeAttributes
     * @param bool $contentNameAsDefault
     * @return string
     */
    public function ezRenderImgTag(
        Content $content,
        string $fieldIdentifier,
        $variationNameOrResponsiveImage,
        array $htmlAttributes = [],
        bool $addSizeAttributes = false,
        bool $contentNameAsDefault = false
    ): string {
        $imageVariationList = [];
        $alt = null;
        $sizesData = [];

        if ($this->fieldHelper->isFieldEmpty($content, $fieldIdentifier)) {
            return '';
        }

        if ($variationNameOrResponsiveImage instanceof ResponsiveImage) {
            foreach ($variationNameOrResponsiveImage->getVariationNameList() as $variationIdentifier) {
                $imageVariationList[] = $this->getImageVariation($content, $fieldIdentifier, $variationIdentifier);
                if ($alt === null) {
                    $alt = $this->getAlt($content, $fieldIdentifier, $contentNameAsDefault);
                }
            }

            $sizesData = $variationNameOrResponsiveImage->getSizesValue();
        } elseif (is_string($variationNameOrResponsiveImage)) {
            $imageVariationList[] = $this->getImageVariation(
                $content,
                $fieldIdentifier,
                $variationNameOrResponsiveImage
            );
            if ($alt === null) {
                $alt = $this->getAlt($content, $fieldIdentifier, $contentNameAsDefault);
            }
        }

        if (!$imageVariationList) {
            return '';
        }

        $attributesStringList = $this->getAttributeStringList(
            $imageVariationList,
            (string)$alt,
            $htmlAttributes,
            $addSizeAttributes,
            is_array($sizesData) ? $sizesData : [$sizesData]
        );

        return '<img ' . implode(' ', $attributesStringList) . ' />';
    }

    /**
     * @param array<mixed> $imageForSrcsetStructList
     * @param mixed $sizesData
     * @param array<string> $htmlAttributes
     * @param bool $addSizeAttributes
     * @param bool $contentNameAsDefault
     * @return string
     */
    public function ezRenderImgTagMultiContent(
        array $imageForSrcsetStructList,
        mixed $sizesData,
        array $htmlAttributes = [],
        bool $addSizeAttributes = false,
        bool $contentNameAsDefault = false
    ): string {
        $imageVariationList = [];
        $alt = null;
        foreach ($imageForSrcsetStructList as $imageForSrcsetStruct) {
            [$content, $fieldIdentifier, $variationIdentifier] = $imageForSrcsetStruct;

            if ($this->fieldHelper->isFieldEmpty($content, $fieldIdentifier)) {
                continue;
            }

            $imageVariationList[] = $this->getImageVariation($content, $fieldIdentifier, $variationIdentifier);

            if ($alt === null) {
                $alt = $this->getAlt($content, $fieldIdentifier, $contentNameAsDefault);
            }
        }

        if (!$imageVariationList) {
            return '';
        }

        $attributesStringList = $this->getAttributeStringList(
            $imageVariationList,
            (string)$alt,
            $htmlAttributes,
            $addSizeAttributes,
            is_array($sizesData) ? $sizesData : [$sizesData]
        );

        return '<img ' . implode(' ', $attributesStringList) . ' />';
    }

    public function getSrc(Content $content, string $fieldIdentifier, string $variationName): string
    {
        $imageVariation = $this->getImageVariation($content, $fieldIdentifier, $variationName);

        return $this->package->getUrl($imageVariation->uri);
    }

    /**
     * @param string[]  $variationNameList
     */
    public function getResponsiveImage(array $variationNameList, mixed $sizesValue): ResponsiveImage
    {
        return new ResponsiveImage($variationNameList, $sizesValue);
    }

    private function getImageVariation(Content $content, string $fieldIdentifier, string $variationName): Variation
    {
        $field = $this->translationHelper->getTranslatedField($content, $fieldIdentifier);
        if (!$field instanceof Field) {
            throw new DomainException('Field does not exists');
        }

        return $this->variationHandler->getVariation(
            $field,
            $content->getVersionInfo(),
            $variationName
        );
    }

    private function getAlt(Content $content, string $fieldIdentifier, bool $contentNameAsDefault): string
    {
        try {
            $imageFieldValue = $content->getFieldValue($fieldIdentifier);
            if ($imageFieldValue instanceof ImageAssetValue) {
                $alt = $imageFieldValue->alternativeText;
            } elseif ($imageFieldValue instanceof ImageValue) {
                $alt = $imageFieldValue->alternativeText;
            } else {
                throw new RuntimeException('Not a valid image field');
            }
        } catch (RuntimeException $e) {
            $alt = '';
        }

        if (empty($alt) && $contentNameAsDefault) {
            $alt = $content->getName();
        }

        return (string)$alt;
    }

    /**
     * @param array<Variation> $imageVariationList
     * @param string $alt
     * @param array<string> $htmlAttributes
     * @param bool $addSizeAttributes
     * @param array<mixed> $sizesData
     * @return array<string>
     */
    private function getAttributeStringList(
        array $imageVariationList,
        string $alt,
        array $htmlAttributes,
        bool $addSizeAttributes,
        array $sizesData
    ): array {
        $firstVariation = $this->getMainVariation($imageVariationList);

        $attributes = [
            'alt' => $alt,
            'loading' => 'lazy',
        ];
        $srcVariation = $this->getSrcVariation($imageVariationList);
        $attributes['src'] = $this->package->getUrl($srcVariation->uri);

        if ($addSizeAttributes && $firstVariation instanceof ImageVariation) {
            $attributes['width'] = $firstVariation->width;
            $attributes['height'] = $firstVariation->height;
        }
        if (count($imageVariationList) > 1) {
            [$attributes['srcset'], $attributes['sizes']] = $this->ezSrcsetAndSizesAttributes(
                $imageVariationList,
                $sizesData,
            );
        }

        $attributes = array_filter(
            array_merge($attributes, $htmlAttributes),
            static function ($value) {
                return $value !== null;
            }
        );

        $attributesStringList = [];
        foreach ($attributes as $attributeName => $attributeValue) {
            $attributesStringList[] = sprintf(
                '%s="%s"',
                twig_escape_filter($this->environment, $attributeName),
                twig_escape_filter($this->environment, $attributeValue, 'html_attr')
            );
        }
        return $attributesStringList;
    }

    /**
     * @param array<Variation> $imageVariationList
     * @param array<mixed> $sizeData
     * @return array{0: string, 1: string}
     * @throws DomainException
     */
    private function ezSrcsetAndSizesAttributes(
        array $imageVariationList,
        array $sizeData
    ): array {
        $srcsetDataList = [];
        foreach ($imageVariationList as $imageVariation) {
            if (!$imageVariation instanceof ImageVariation) {
                throw new DomainException('Unexpected imageVariation type');
            }

            $srcsetDataList[] = sprintf(
                '%s %dw',
                $this->package->getUrl($imageVariation->uri),
                $imageVariation->width
            );
        }

        return [implode(',', $srcsetDataList), implode(',', $this->getSizeVariation($sizeData))];
    }

    /**
     * @param Variation[] $imageVariationList
     * @return ImageVariation
     */
    private function getMainVariation(array $imageVariationList): ImageVariation
    {
        return array_reduce($imageVariationList, function ($carry, $variation) {
            if ($carry === null || $variation->width > $carry->width) {
                return $variation;
            } else {
                return $carry;
            }
        });
    }

    /**
     * @param Variation[] $imageVariationList
     * @return ImageVariation
     */
    private function getSrcVariation(array $imageVariationList): ImageVariation
    {
        return array_reduce($imageVariationList, function ($carry, $variation) {
            if ($carry === null || $variation->width < $carry->width) {
                return $variation;
            } else {
                return $carry;
            }
        });
    }

    /**
     * @param array<mixed> $sizeData
     * @return array<string>
     */
    private function getSizeVariation(array $sizeData): array
    {
        return array_map(function ($size) {
            return is_array($size) ? implode(' ', array_reverse($size)) : $size;
        }, $sizeData);
    }
}
