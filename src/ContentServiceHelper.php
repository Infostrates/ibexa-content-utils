<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentUtils;

use Ibexa\Contracts\Core\Repository\ContentService;
use Ibexa\Contracts\Core\Repository\Exceptions\NotFoundException as ApiNotFoundException;
use Ibexa\Contracts\Core\Repository\Exceptions\UnauthorizedException as ApiUnauthorizedException;
use Ibexa\Contracts\Core\Repository\LanguageResolver;
use Ibexa\Contracts\Core\Repository\Values\Content\Content;

class ContentServiceHelper
{
    private ContentService $contentService;
    private LanguageResolver $languageResolver;

    public function __construct(ContentService $contentService, LanguageResolver $languageResolver)
    {
        $this->contentService = $contentService;
        $this->languageResolver = $languageResolver;
    }

    /**
     * @param mixed $contentId
     * @return Content
     */
    public function loadContent($contentId, bool $applyVisibility = false): ?Content
    {
        try {
            $content = $this->contentService->loadContent(
                $contentId,
                $this->languageResolver->getPrioritizedLanguages()
            );

            return ($applyVisibility && $content->contentInfo->isHidden) ? null : $content;
        } catch (ApiNotFoundException | ApiUnauthorizedException $e) {
            return null;
        }
    }

    public function loadContentByRemoteId(string $remoteId, bool $applyVisibility = false): ?Content
    {
        try {
            $content = $this->contentService->loadContentByRemoteId(
                $remoteId,
                $this->languageResolver->getPrioritizedLanguages()
            );

            return ($applyVisibility && $content->contentInfo->isHidden) ? null : $content;
        } catch (ApiNotFoundException | ApiUnauthorizedException $e) {
            return null;
        }
    }

    /**
     * @param Content[] $contentList
     * @return int[]
     */
    public function getContentIdsFromContentList(array $contentList): array
    {
        return array_map(static function (Content $content) {
            return $content->id;
        }, $contentList);
    }
}
