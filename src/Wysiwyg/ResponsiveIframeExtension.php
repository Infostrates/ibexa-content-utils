<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentUtils\Wysiwyg;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class ResponsiveIframeExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            new TwigFilter('responsive_iframe', [$this, 'responsiveIframeFilter'], ['is_safe' => ['html']]),
        ];
    }

    public function responsiveIframeFilter(string $string): string
    {
        $string = str_ireplace(
            ['<iframe', '/iframe>'],
            ['<div class="responsive-embed widescreen"><iframe', '/iframe></div>'],
            $string
        );

        return (string)preg_replace('#<iframe[^>]*/>#i', '$0</div>', $string);
    }
}
