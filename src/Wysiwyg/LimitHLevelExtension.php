<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentUtils\Wysiwyg;

use LogicException;
use RuntimeException;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class LimitHLevelExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            new TwigFilter('limit_h_level', [$this, 'limitHLevel']),
        ];
    }

    /**
     * @param string $content
     * @param int    $minLevel
     * @return string
     * @throws RuntimeException
     */
    public function limitHLevel(string $content, int $minLevel): string
    {
        for ($i = 9; $i >= 1; $i--) {
            $content = preg_replace('#<(/?) *h' . $i . '#', '<$1h' . ($i + $minLevel - 1), $content);
            if ($content === null) {
                throw new RuntimeException(sprintf('Unable to apply minLevel %d to : %s', $minLevel, $content));
            }
            if (!is_string($content)) {
                throw new LogicException('Unexpected content type');
            }
        }

        return $content;
    }
}
