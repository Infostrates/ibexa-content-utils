<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentUtils;

use Symfony\Component\HttpKernel\Bundle\Bundle;

final class InfostratesIbexaContentUtilsBundle extends Bundle
{
}
