<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentUtils;

use Infostrates\IbexaContentUtils\Traits\IsFieldValueSeemsToBeByLanguage;
use DomainException;
use Ibexa\Contracts\Core\Repository\Exceptions\BadStateException;
use Ibexa\Contracts\Core\Repository\Exceptions\ContentFieldValidationException;
use Ibexa\Contracts\Core\Repository\Exceptions\ContentValidationException;
use Ibexa\Contracts\Core\Repository\Exceptions\InvalidArgumentException;
use Ibexa\Contracts\Core\Repository\Exceptions\NotFoundException;
use Ibexa\Contracts\Core\Repository\Exceptions\UnauthorizedException;
use Ibexa\Contracts\Core\Repository\Repository;
use Ibexa\Contracts\Core\Repository\Values\Content\Content;
use Ibexa\Contracts\Core\Repository\Values\Content\Location;
use Ibexa\Contracts\Core\FieldType\Value;

class RepositoryWriteHelper
{
    use IsFieldValueSeemsToBeByLanguage;

    private Repository $repository;

    public function __construct(Repository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Location                                                $parentLocation
     * @param string                                                  $contentTypeIdentifier
     * @param string                                                  $languageCode
     * @param array<string, string|Value|array<string, string|Value>> $fieldValueByIdentifier
     * @param string|null                                             $remoteId
     * @param int                                                     $priority
     * @return Content
     */
    public function createContent(
        Location $parentLocation,
        string $contentTypeIdentifier,
        string $languageCode,
        array $fieldValueByIdentifier,
        ?string $remoteId,
        int $priority = 0
    ): Content {
        try {
            $contentType = $this->repository->getContentTypeService()
                ->loadContentTypeByIdentifier($contentTypeIdentifier);
            $contentCreateStruct = $this->repository->getContentService()->newContentCreateStruct(
                $contentType,
                $languageCode
            );
            foreach ($fieldValueByIdentifier as $fieldIdentifier => $fieldValueOrFieldValueByLanguage) {
                if (
                    is_array($fieldValueOrFieldValueByLanguage)
                    && $this->isFieldValueSeemsToBeByLanguage($fieldValueOrFieldValueByLanguage)
                ) {
                    foreach ($fieldValueOrFieldValueByLanguage as $fieldLanguageCode => $fieldValue) {
                        $contentCreateStruct->setField($fieldIdentifier, $fieldValue, $fieldLanguageCode);
                    }
                } else {
                    $contentCreateStruct->setField($fieldIdentifier, $fieldValueOrFieldValueByLanguage, $languageCode);
                }
            }
            if ($remoteId) {
                $contentCreateStruct->remoteId = $remoteId;
            }
            $contentCreateStruct->alwaysAvailable = $contentType->defaultAlwaysAvailable;

            $locationCreateStruct = $this->repository->getLocationService()
                ->newLocationCreateStruct($parentLocation->id);
            if ($remoteId) {
                $locationCreateStruct->remoteId = $remoteId;
            }
            $locationCreateStruct->sortField = $contentType->defaultSortField;
            $locationCreateStruct->sortOrder = $contentType->defaultSortOrder;
            $locationCreateStruct->priority = $priority;

            $contentDraft = $this->repository->getContentService()->createContent(
                $contentCreateStruct,
                [$locationCreateStruct]
            );

            return $this->repository->getContentService()->publishVersion($contentDraft->versionInfo);
        } catch (ContentFieldValidationException $e) {
            throw new \InvalidArgumentException(
                'Content Field validation failed : ' . var_export($e->getFieldErrors(), true)
            );
        } catch (
            BadStateException | ContentValidationException | InvalidArgumentException
            | NotFoundException | UnauthorizedException $e
        ) {
            throw new DomainException('Unable to create content', $e->getCode(), $e);
        }
    }

    public function moveLocation(Location $location, Location $newParentLocation): void
    {
        try {
            $this->repository->getLocationService()->moveSubtree($location, $newParentLocation);
        } catch (InvalidArgumentException | UnauthorizedException $e) {
            throw new DomainException('Unable to move location', $e->getCode(), $e);
        }
    }

    /**
     * @param Content                                                 $content
     * @param string                                                  $languageCode
     * @param array<string, string|Value|array<string, string|Value>> $fieldValueByIdentifier
     */
    public function updateContent(Content $content, string $languageCode, array $fieldValueByIdentifier): void
    {
        $updateStruct = $this->repository->getContentService()->newContentUpdateStruct();
        foreach ($fieldValueByIdentifier as $fieldIdentifier => $fieldValueOrFieldValueByLanguage) {
            if (
                is_array($fieldValueOrFieldValueByLanguage)
                && $this->isFieldValueSeemsToBeByLanguage($fieldValueOrFieldValueByLanguage)
            ) {
                foreach ($fieldValueOrFieldValueByLanguage as $fieldLanguageCode => $fieldValue) {
                    $updateStruct->setField($fieldIdentifier, $fieldValue, $fieldLanguageCode);
                }
            } else {
                $updateStruct->setField($fieldIdentifier, $fieldValueOrFieldValueByLanguage, $languageCode);
            }
        }

        try {
            $contentDraft = $this->repository->getContentService()->createContentDraft($content->contentInfo);
            $contentDraft = $this->repository->getContentService()->updateContent(
                $contentDraft->versionInfo,
                $updateStruct
            );
            $this->repository->getContentService()->publishVersion($contentDraft->versionInfo);
        } catch (ContentFieldValidationException $e) {
            throw new \InvalidArgumentException(
                'Content Field validation failed : ' . var_export($e->getFieldErrors(), true)
            );
        } catch (BadStateException | ContentValidationException | InvalidArgumentException | UnauthorizedException $e) {
            throw new DomainException('Unable to update content ' . $content->id, $e->getCode(), $e);
        }
    }
}
