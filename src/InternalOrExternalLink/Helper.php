<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentUtils\InternalOrExternalLink;

use Exception;
use Infostrates\IbexaContentUtils\InternalOrExternalLink\Model\Link;
use BadMethodCallException;
use DomainException;
use Ibexa\Contracts\Core\Repository\Values\Content\Content;
use Ibexa\Contracts\Core\Repository\Values\Content\Location;
use Ibexa\Core\FieldType;
use Ibexa\Core\Helper\TranslationHelper;
use Symfony\Component\Routing\RouterInterface;

class Helper
{
    private TranslationHelper $translationHelper;
    private RouterInterface $router;

    /**
     * @param TranslationHelper $translationHelper
     * @param RouterInterface   $router
     */
    public function __construct(TranslationHelper $translationHelper, RouterInterface $router)
    {
        $this->translationHelper = $translationHelper;
        $this->router = $router;
    }

    /**
     * @param Content  $content
     * @param string   $prefix
     * @param int|null $targetContentId If the link is internal, will return the id of the targeted content
     * @return Link|null
     */
    public function getInternalOrExternalLink(
        Content $content,
        string $prefix = '',
        ?int &$targetContentId = null
    ): ?Link {
        if ($prefix !== '') {
            $prefix .= '_';
        }

        $internalLinkField = $this->translationHelper->getTranslatedField($content, $prefix . 'internal_link');
        if ($internalLinkField) {
            $internalLinkFieldValue = $internalLinkField->value;
            if (!$internalLinkFieldValue instanceof FieldType\RelationList\Value) {
                throw new DomainException($prefix . 'internal_link should be of type relation list');
            }
        }

        $externalLinkField = $this->translationHelper->getTranslatedField($content, $prefix . 'external_link');
        $externalLinkFieldValue = $externalLinkField->value ?? null;

        if (isset($internalLinkFieldValue, $internalLinkFieldValue->destinationContentIds[0])) {
            $internalLinkComplementField = $this->translationHelper->getTranslatedField(
                $content,
                $prefix . 'internal_link_complement'
            );
            $internalLinkComplementString = '';
            if ($internalLinkComplementField) {
                $internalLinkComplementFieldValue = $internalLinkComplementField->value;
                if ($internalLinkComplementFieldValue instanceof FieldType\TextLine\Value) {
                    $internalLinkComplementString = $internalLinkComplementFieldValue->text;
                }
            }

            return $this->getLinkFromContentId(
                (int)$internalLinkFieldValue->destinationContentIds[0],
                $internalLinkComplementString
            );
        }

        if (!$externalLinkFieldValue instanceof FieldType\TextLine\Value || empty($externalLinkFieldValue->text)) {
            return null;
        }

        return new Link(
            $externalLinkFieldValue->text,
            $externalLinkFieldValue->text[0] === '#' ? null : Link::TARGET_BLANK,
            'external'
        );
    }

    public function getLinkForLocation(Location $location, bool $forceTargetBlank = false, string $urlSuffix = ''): Link
    {
        return new Link(
            $this->router->generate('ibexa.url.alias', ['locationId' => $location->id]) . $urlSuffix,
            $forceTargetBlank ? Link::TARGET_BLANK : null
        );
    }

    public function getLinkForExternalLink(string $externalLink): Link
    {
        return new Link($externalLink, Link::TARGET_BLANK);
    }

    public function getLinkFromContentId(
        int $targetContentId,
        string $urlSuffix = '',
        bool $forceTargetBlank = false
    ): ?Link {
        try {
            return new Link(
                $this->router->generate(
                    'ibexa.url.alias',
                    ['contentId' => $targetContentId]
                )
                . $urlSuffix,
                $forceTargetBlank ? Link::TARGET_BLANK : null
            );
        } catch (Exception $e) {
            return null;
        }
    }
}
