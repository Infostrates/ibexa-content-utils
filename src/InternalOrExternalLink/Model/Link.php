<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentUtils\InternalOrExternalLink\Model;

class Link
{
    public const TARGET_BLANK = '_blank';

    private string $url;
    private ?string $target;
    private string $type;

    /**
     * @param string $url
     * @param string|null $target
     * @param string $type
     */
    public function __construct(string $url, ?string $target = null, string $type = 'internal')
    {
        $this->url = $url;
        $this->target = $target;
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string|null
     */
    public function getTarget(): ?string
    {
        return $this->target;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }
}
