<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentUtils\InternalOrExternalLink\TwigExtension;

use Infostrates\IbexaContentUtils\InternalOrExternalLink\Helper;
use Infostrates\IbexaContentUtils\InternalOrExternalLink\Model\Link;
use Ibexa\Contracts\Core\Repository\Values\Content\Content;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class EzInternalOrExternalLink extends AbstractExtension
{
    private Helper $helper;

    /**
     * @param Helper $helper
     */
    public function __construct(Helper $helper)
    {
        $this->helper = $helper;
    }

    /**
     * @return array<TwigFunction>
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('ez_internal_or_external_link', [$this, 'ezInternalOrExternalLink']),
            new TwigFunction('link_attr', [$this, 'linkAttr'], ['is_safe' => ['html_attr']]),
        ];
    }

    /**
     * @param Content $content
     * @param string  $fieldPrefix
     * @return Link|null
     */
    public function ezInternalOrExternalLink(Content $content, string $fieldPrefix = ''): ?Link
    {
        return $this->helper->getInternalOrExternalLink($content, $fieldPrefix);
    }

    public function linkAttr(?Link $link, ?string $forcedTarget = null): string
    {
        if (!$link) {
            return '';
        }

        $string = 'href="' . htmlspecialchars($link->getUrl()) . '"';
        $target = $forcedTarget ?? $link->getTarget();
        if ($target) {
            $string .= ' target="' . $target . '"';
        }

        return $string;
    }
}
