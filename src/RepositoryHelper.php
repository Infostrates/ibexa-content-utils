<?php

namespace Infostrates\IbexaContentUtils;

use Ibexa\Contracts\Core\Repository\Exceptions\NotFoundException as ApiNotFoundException;
use Ibexa\Contracts\Core\Repository\Values\Content\Content;
use Ibexa\Contracts\Core\Repository\Values\Content\Location;
use Ibexa\Core\FieldType;
use Pagerfanta\Pagerfanta;
use Ibexa\Contracts\Core\Repository\Values\Content\Query\Criterion;
use Ibexa\Contracts\Core\Repository\Values\Content\Query\SortClause;

class RepositoryHelper
{
    private ContentServiceHelper $contentServiceHelper;
    private LocationServiceHelper $locationServiceHelper;
    private SearchServiceHelper $searchServiceHelper;
    private FieldHelper $fieldHelper;

    public function __construct(
        ContentServiceHelper $contentServiceHelper,
        LocationServiceHelper $locationServiceHelper,
        SearchServiceHelper $searchServiceHelper,
        FieldHelper $fieldHelper
    ) {
        $this->contentServiceHelper = $contentServiceHelper;
        $this->locationServiceHelper = $locationServiceHelper;
        $this->searchServiceHelper = $searchServiceHelper;
        $this->fieldHelper = $fieldHelper;
    }

    /**
     * @param Location             $location
     * @param null|string|string[] $contentTypeIdentifierList
     * @param Criterion[]          $additionalCriterionList
     * @param int                  $limit
     * @param int                  $offset
     * @param null|SortClause[]    $sortClauseList
     * @param bool                 $ignoreVisibility
     * @return Location[]
     * @deprecated Will be removed in 3.0. Use ContentSearchHelper::loadAnyDepthChildrenLocationList() instead.
     */
    public function loadAnyDepthChildrenLocationList(
        Location $location,
        $contentTypeIdentifierList = null,
        array $additionalCriterionList = [],
        int $limit = 0,
        int $offset = 0,
        ?array $sortClauseList = null,
        bool $ignoreVisibility = false
    ): array {
        return $this->searchServiceHelper->loadAnyDepthChildrenLocationList(
            $location,
            $contentTypeIdentifierList,
            $additionalCriterionList,
            $limit,
            $offset,
            $sortClauseList,
            $ignoreVisibility
        );
    }

    /**
     * @param Location             $location
     * @param null|string|string[] $contentTypeIdentifierList
     * @param Criterion[]          $additionalCriterionList
     * @param null|SortClause[]    $sortClauseList
     * @param bool                 $ignoreVisibility
     * @return Location|null
     * @deprecated Will be removed in 3.0. Use ContentSearchHelper::loadAnyDepthChildrenSingleLocation() instead.
     */
    public function loadAnyDepthChildrenSingleLocation(
        Location $location,
        $contentTypeIdentifierList = null,
        array $additionalCriterionList = [],
        ?array $sortClauseList = null,
        bool $ignoreVisibility = false
    ): ?Location {
        return $this->searchServiceHelper->loadAnyDepthChildrenSingleLocation(
            $location,
            $contentTypeIdentifierList,
            $additionalCriterionList,
            $sortClauseList,
            $ignoreVisibility
        );
    }

    /**
     * @param Location             $location
     * @param null|string|string[] $contentTypeIdentifierList
     * @param Criterion[]          $additionalCriterionList
     * @param int                  $limit
     * @param int                  $offset
     * @param null|SortClause[]    $sortClauseList
     * @param bool                 $ignoreVisibility
     * @return Content[]
     * @deprecated Will be removed in 3.0. Use ContentSearchHelper::loadAnyDepthChildrenContentList() instead.
     */
    public function loadAnyDepthChildrenContentList(
        Location $location,
        $contentTypeIdentifierList = null,
        array $additionalCriterionList = [],
        int $limit = 0,
        int $offset = 0,
        ?array $sortClauseList = null,
        bool $ignoreVisibility = false
    ): array {
        return $this->searchServiceHelper->loadAnyDepthChildrenContentList(
            $location,
            $contentTypeIdentifierList,
            $additionalCriterionList,
            $limit,
            $offset,
            $sortClauseList,
            $ignoreVisibility
        );
    }

    /**
     * @param Location             $location
     * @param null|string|string[] $contentTypeIdentifierList
     * @param Criterion[]          $additionalCriterionList
     * @param null|SortClause[]    $sortClauseList
     * @param bool                 $ignoreVisibility
     * @return Content|null
     * @deprecated Will be removed in 3.0. Use ContentSearchHelper::loadAnyDepthChildrenSingleContent() instead.
     */
    public function loadAnyDepthChildrenSingleContent(
        Location $location,
        $contentTypeIdentifierList = null,
        array $additionalCriterionList = [],
        ?array $sortClauseList = null,
        bool $ignoreVisibility = false
    ): ?Content {
        return $this->searchServiceHelper->loadAnyDepthChildrenSingleContent(
            $location,
            $contentTypeIdentifierList,
            $additionalCriterionList,
            $sortClauseList,
            $ignoreVisibility
        );
    }

    /**
     * @param Location             $location
     * @param null|string|string[] $contentTypeIdentifierList
     * @param Criterion[]          $additionalCriterionList
     * @param int                  $limit
     * @param int                  $offset
     * @param null|SortClause[]    $sortClauseList
     * @param bool                 $ignoreVisibility
     * @return Location[]
     * @deprecated Will be removed in 3.0. Use ContentSearchHelper::loadDirectChildrenLocationList() instead.
     */
    public function loadDirectChildrenLocationList(
        Location $location,
        $contentTypeIdentifierList = null,
        array $additionalCriterionList = [],
        int $limit = 0,
        int $offset = 0,
        ?array $sortClauseList = null,
        bool $ignoreVisibility = false
    ): array {
        return $this->searchServiceHelper->loadDirectChildrenLocationList(
            $location,
            $contentTypeIdentifierList,
            $additionalCriterionList,
            $limit,
            $offset,
            $sortClauseList,
            $ignoreVisibility
        );
    }

    /**
     * @param Location             $location
     * @param null|string|string[] $contentTypeIdentifierList
     * @param Criterion[]          $additionalCriterionList
     * @param null|SortClause[]    $sortClauseList
     * @param bool                 $ignoreVisibility
     * @return Location|null
     * @deprecated Will be removed in 3.0. Use ContentSearchHelper::loadDirectChildrenSingleLocation() instead.
     */
    public function loadDirectChildrenSingleLocation(
        Location $location,
        $contentTypeIdentifierList = null,
        array $additionalCriterionList = [],
        ?array $sortClauseList = null,
        bool $ignoreVisibility = false
    ): ?Location {
        return $this->searchServiceHelper->loadDirectChildrenSingleLocation(
            $location,
            $contentTypeIdentifierList,
            $additionalCriterionList,
            $sortClauseList,
            $ignoreVisibility
        );
    }

    /**
     * @param Location             $location
     * @param null|string|string[] $contentTypeIdentifierList
     * @param Criterion[]          $additionalCriterionList
     * @param int                  $limit
     * @param int                  $offset
     * @param null|SortClause[]    $sortClauseList
     * @param bool                 $ignoreVisibility
     * @return Content[]
     * @deprecated Will be removed in 3.0. Use ContentSearchHelper::loadDirectChildrenContentList() instead.
     */
    public function loadDirectChildrenContentList(
        Location $location,
        $contentTypeIdentifierList = null,
        array $additionalCriterionList = [],
        int $limit = 0,
        int $offset = 0,
        ?array $sortClauseList = null,
        bool $ignoreVisibility = false
    ): array {
        return $this->searchServiceHelper->loadDirectChildrenContentList(
            $location,
            $contentTypeIdentifierList,
            $additionalCriterionList,
            $limit,
            $offset,
            $sortClauseList,
            $ignoreVisibility
        );
    }

    /**
     * @param Location             $location
     * @param null|string|string[] $contentTypeIdentifierList
     * @param Criterion[]          $additionalCriterionList
     * @param null|SortClause[]    $sortClauseList
     * @param bool                 $ignoreVisibility
     * @return Content|null
     * @deprecated Will be removed in 3.0. Use ContentSearchHelper::loadDirectChildrenSingleContent() instead.
     */
    public function loadDirectChildrenSingleContent(
        Location $location,
        $contentTypeIdentifierList = null,
        array $additionalCriterionList = [],
        ?array $sortClauseList = null,
        bool $ignoreVisibility = false
    ): ?Content {
        return $this->searchServiceHelper->loadDirectChildrenSingleContent(
            $location,
            $contentTypeIdentifierList,
            $additionalCriterionList,
            $sortClauseList,
            $ignoreVisibility
        );
    }

    /**
     * @param mixed $contentId
     * @return Content
     * @deprecated Will be removed in 3.0. Use ContentServiceHelper::loadContent() instead.
     */
    public function loadContent($contentId): ?Content
    {
        return $this->contentServiceHelper->loadContent($contentId);
    }

    /**
     * @param string $remoteId
     * @return Content|null
     * @deprecated Will be removed in 3.0. Use ContentServiceHelper::loadContentByRemoteId() instead.
     */
    public function loadContentByRemoteId(string $remoteId): ?Content
    {
        return $this->contentServiceHelper->loadContentByRemoteId($remoteId);
    }

    /**
     * @param int $locationId
     * @return Location|null
     * @deprecated Will be removed in 3.0. Use LocationServiceHelper::loadLocation() instead.
     */
    public function loadLocation(int $locationId): ?Location
    {
        return $this->locationServiceHelper->loadLocation($locationId);
    }

    /**
     * @param string $remoteId
     * @return Location|null
     * @deprecated Will be removed in 3.0. Use LocationServiceHelper::loadLocationByRemoteId() instead.
     */
    public function loadLocationByRemoteId(string $remoteId): ?Location
    {
        return $this->locationServiceHelper->loadLocationByRemoteId($remoteId);
    }

    /**
     * @param string $contentTypeIdentifier
     * @param string $selectionFieldIdentifier
     * @return array<string, string> de la forme ['id' => 'label', 'id2' => 'label2', ...]
     * @throws ApiNotFoundException
     * @deprecated Will be removed in 3.0. Use FieldHelper::getSelectionLabelMapping() instead.
     */
    public function getSelectionLabelMapping(string $contentTypeIdentifier, string $selectionFieldIdentifier): array
    {
        return $this->fieldHelper->getSelectionLabelMapping($contentTypeIdentifier, $selectionFieldIdentifier);
    }

    /**
     * @param FieldType\RelationList\Value $relationListValue
     * @return Content[]
     * @deprecated Will be removed in 3.0. Use FieldHelper::loadContentListFromRelationListValue() instead.
     */
    public function loadContentListFromRelationListValue(FieldType\RelationList\Value $relationListValue): array
    {
        return $this->fieldHelper->loadContentListFromRelationListValue($relationListValue);
    }

    /**
     * @param FieldType\RelationList\Value $relationListValue
     * @return Content
     * @deprecated Will be removed in 3.0. Use FieldHelper::loadSingleContentFromRelationListValue() instead.
     */
    public function loadSingleContentFromRelationListValue(FieldType\RelationList\Value $relationListValue): ?Content
    {
        return $this->fieldHelper->loadSingleContentFromRelationListValue($relationListValue);
    }

    /**
     * @param Location             $location
     * @param null|string|string[] $contentTypeIdentifierList
     * @param Criterion[]          $additionalCriterionList
     * @param null|SortClause[]    $sortClauseList
     * @return Pagerfanta<Content>
     * @deprecated Will be removed in 3.0. Use ContentSearchHelper::loadDirectChildrenContentPager() instead.
     */
    public function loadDirectChildrenContentPager(
        Location $location,
        $contentTypeIdentifierList = null,
        array $additionalCriterionList = [],
        ?array $sortClauseList = null
    ): Pagerfanta {
        return $this->searchServiceHelper->loadDirectChildrenContentPager(
            $location,
            $contentTypeIdentifierList,
            $additionalCriterionList,
            $sortClauseList
        );
    }

    /**
     * @param Location             $location
     * @param null|string|string[] $contentTypeIdentifierList
     * @param Criterion[]          $additionalCriterionList
     * @param null|SortClause[]    $sortClauseList
     * @return Pagerfanta<Content>
     * @deprecated Will be removed in 3.0. Use ContentSearchHelper::loadAnyDepthChildrenContentPager() instead.
     */
    public function loadAnyDepthChildrenContentPager(
        Location $location,
        $contentTypeIdentifierList = null,
        array $additionalCriterionList = [],
        ?array $sortClauseList = null
    ): Pagerfanta {
        return $this->searchServiceHelper->loadAnyDepthChildrenContentPager(
            $location,
            $contentTypeIdentifierList,
            $additionalCriterionList,
            $sortClauseList
        );
    }

    /**
     * @param string       $contentTypeIdentifier
     * @param int          $limit
     * @param int          $offset
     * @param SortClause[] $sortClauseList
     * @param Criterion[]  $additionalCriterionList
     * @return Content[]
     * @deprecated Will be removed in 3.0. Use ContentSearchHelper::loadContentList() instead.
     */
    public function loadContentList(
        string $contentTypeIdentifier,
        int $limit = 0,
        int $offset = 0,
        array $sortClauseList = [],
        array $additionalCriterionList = []
    ): array {
        return $this->searchServiceHelper->loadContentList(
            $contentTypeIdentifier,
            $limit,
            $offset,
            $sortClauseList,
            $additionalCriterionList
        );
    }

    /**
     * @param Content[] $contentList
     * @return int[]
     * @deprecated Will be removed in 3.0. Use ContentServiceHelper::getContentIdsFromContentList() instead.
     */
    public function getContentIdsFromContentList(array $contentList): array
    {
        return $this->contentServiceHelper->getContentIdsFromContentList($contentList);
    }

    /**
     * @return int
     * @deprecated Will be removed in 3.0. Use LocationServiceHelper::getRootLocationId() instead.
     */
    public function getRootLocationId(): int
    {
        return $this->locationServiceHelper->getRootLocationId();
    }

    /**
     * @return Location
     * @deprecated Will be removed in 3.0. Use LocationServiceHelper::loadRootLocation() instead.
     */
    public function loadRootLocation(): Location
    {
        return $this->locationServiceHelper->loadRootLocation();
    }
}
