<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentUtils;

use DomainException;
use Ibexa\Contracts\Core\Repository\Exceptions\InvalidArgumentException as APIInvalidArgumentExceptionAlias;
use Ibexa\Contracts\Core\Repository\LanguageResolver;
use Ibexa\Contracts\Core\Repository\LocationService;
use Ibexa\Contracts\Core\Repository\SearchService;
use Ibexa\Contracts\Core\Repository\Values\Content\Content;
use Ibexa\Contracts\Core\Repository\Values\Content\Location;
use Ibexa\Contracts\Core\Repository\Values\Content\LocationQuery;
use Ibexa\Contracts\Core\Repository\Values\Content\Query;
use Ibexa\Contracts\Core\Repository\Values\Content\Query\Criterion;
use Ibexa\Contracts\Core\Repository\Values\Content\Query\SortClause;
use Ibexa\Contracts\Core\Repository\Values\Content\Search\SearchHit;
use Ibexa\Contracts\Core\Repository\Values\Content\Search\SearchResult;
use Ibexa\Core\Pagination\Pagerfanta\ContentSearchAdapter;
use Ibexa\Core\Pagination\Pagerfanta\LocationSearchAdapter;
use InvalidArgumentException;
use Pagerfanta\Pagerfanta;

class SearchServiceHelper
{
    public const REALLY_HIGH_LIMIT = 100000;
    public const REASONABLY_HIGH_LIMIT = 1000;

    private SearchService $searchService;
    private LanguageResolver $languageResolver;

    public function __construct(SearchService $searchService, LanguageResolver $languageResolver)
    {
        $this->searchService = $searchService;
        $this->languageResolver = $languageResolver;
    }

    /**
     * @param Location             $location
     * @param null|string|string[] $contentTypeIdentifierList
     * @param Criterion[]          $additionalCriterionList
     * @param int                  $limit
     * @param int                  $offset
     * @param null|SortClause[]    $sortClauseList
     * @param bool                 $ignoreVisibility
     * @return Location[]
     */
    public function loadAnyDepthChildrenLocationList(
        Location $location,
        $contentTypeIdentifierList = null,
        array $additionalCriterionList = [],
        int $limit = 0,
        int $offset = 0,
        ?array $sortClauseList = null,
        bool $ignoreVisibility = false
    ): array {
        $query = $this->getAnyDepthChildrenLocationQuery(
            $location,
            $contentTypeIdentifierList,
            $limit,
            $offset,
            $sortClauseList,
            $additionalCriterionList,
            $ignoreVisibility
        );

        try {
            $searchResult = $this->searchService->findLocations($query);
        } catch (APIInvalidArgumentExceptionAlias $e) {
            throw new InvalidArgumentException('Bad query', $e->getCode(), $e);
        }

        return $this->fetchLocationListFromSearchResult($searchResult);
    }

    /**
     * @param Location             $location
     * @param null|string|string[] $contentTypeIdentifierList
     * @param Criterion[]          $additionalCriterionList
     * @param null|SortClause[]    $sortClauseList
     * @param bool                 $ignoreVisibility
     * @return Location|null
     */
    public function loadAnyDepthChildrenSingleLocation(
        Location $location,
        $contentTypeIdentifierList = null,
        array $additionalCriterionList = [],
        ?array $sortClauseList = null,
        bool $ignoreVisibility = false
    ): ?Location {
        $query = $this->getAnyDepthChildrenLocationQuery(
            $location,
            $contentTypeIdentifierList,
            1,
            0,
            $sortClauseList,
            $additionalCriterionList,
            $ignoreVisibility
        );

        try {
            return $this->fetchSingleLocationFromSearchResult(
                $this->searchService->findLocations($query)
            );
        } catch (APIInvalidArgumentExceptionAlias $e) {
            throw new InvalidArgumentException('Bad query', $e->getCode(), $e);
        }
    }

    /**
     * @param Location             $location
     * @param null|string|string[] $contentTypeIdentifierList
     * @param Criterion[]          $additionalCriterionList
     * @param int                  $limit
     * @param int                  $offset
     * @param null|SortClause[]    $sortClauseList
     * @param bool                 $ignoreVisibility
     * @return Content[]
     */
    public function loadAnyDepthChildrenContentList(
        Location $location,
        $contentTypeIdentifierList = null,
        array $additionalCriterionList = [],
        int $limit = 0,
        int $offset = 0,
        ?array $sortClauseList = null,
        bool $ignoreVisibility = false
    ): array {
        $query = $this->getAnyDepthChildrenLocationQuery(
            $location,
            $contentTypeIdentifierList,
            $limit,
            $offset,
            $sortClauseList,
            $additionalCriterionList,
            $ignoreVisibility
        );

        try {
            $searchResult = $this->searchService->findLocations($query);
        } catch (APIInvalidArgumentExceptionAlias $e) {
            throw new InvalidArgumentException('Bad query', $e->getCode(), $e);
        }

        return $this->fetchContentListFromSearchResult($searchResult);
    }

    /**
     * @param Location             $location
     * @param null|string|string[] $contentTypeIdentifierList
     * @param Criterion[]          $additionalCriterionList
     * @param null|SortClause[]    $sortClauseList
     * @param bool                 $ignoreVisibility
     * @return Content|null
     */
    public function loadAnyDepthChildrenSingleContent(
        Location $location,
        $contentTypeIdentifierList = null,
        array $additionalCriterionList = [],
        ?array $sortClauseList = null,
        bool $ignoreVisibility = false
    ): ?Content {
        $query = $this->getAnyDepthChildrenLocationQuery(
            $location,
            $contentTypeIdentifierList,
            1,
            0,
            $sortClauseList,
            $additionalCriterionList,
            $ignoreVisibility
        );

        try {
            return $this->fetchSingleContentFromSearchResult(
                $this->searchService->findLocations($query)
            );
        } catch (APIInvalidArgumentExceptionAlias $e) {
            throw new InvalidArgumentException('Bad query', $e->getCode(), $e);
        }
    }

    /**
     * @param Location             $location
     * @param null|string|string[] $contentTypeIdentifierList
     * @param Criterion[]          $additionalCriterionList
     * @param int                  $limit
     * @param int                  $offset
     * @param null|SortClause[]    $sortClauseList
     * @param bool                 $ignoreVisibility
     * @return Location[]
     */
    public function loadDirectChildrenLocationList(
        Location $location,
        $contentTypeIdentifierList = null,
        array $additionalCriterionList = [],
        int $limit = 0,
        int $offset = 0,
        ?array $sortClauseList = null,
        bool $ignoreVisibility = false
    ): array {
        $query = $this->getDirectChildrenLocationQuery(
            $location,
            $contentTypeIdentifierList,
            $limit,
            $offset,
            $sortClauseList,
            $additionalCriterionList,
            $ignoreVisibility
        );

        try {
            return $this->fetchLocationListFromSearchResult(
                $this->searchService->findLocations($query)
            );
        } catch (APIInvalidArgumentExceptionAlias $e) {
            throw new InvalidArgumentException('Bad query', $e->getCode(), $e);
        }
    }

    /**
     * @param Location             $location
     * @param null|string|string[] $contentTypeIdentifierList
     * @param Criterion[]          $additionalCriterionList
     * @param null|SortClause[]    $sortClauseList
     * @param bool                 $ignoreVisibility
     * @return Location|null
     */
    public function loadDirectChildrenSingleLocation(
        Location $location,
        $contentTypeIdentifierList = null,
        array $additionalCriterionList = [],
        ?array $sortClauseList = null,
        bool $ignoreVisibility = false
    ): ?Location {
        $query = $this->getDirectChildrenLocationQuery(
            $location,
            $contentTypeIdentifierList,
            1,
            0,
            $sortClauseList,
            $additionalCriterionList,
            $ignoreVisibility
        );

        try {
            return $this->fetchSingleLocationFromSearchResult(
                $this->searchService->findLocations($query)
            );
        } catch (APIInvalidArgumentExceptionAlias $e) {
            throw new InvalidArgumentException('Bad query', $e->getCode(), $e);
        }
    }

    /**
     * @param Location             $location
     * @param null|string|string[] $contentTypeIdentifierList
     * @param Criterion[]          $additionalCriterionList
     * @param int                  $limit
     * @param int                  $offset
     * @param null|SortClause[]    $sortClauseList
     * @param bool                 $ignoreVisibility
     * @return Content[]
     */
    public function loadDirectChildrenContentList(
        Location $location,
        $contentTypeIdentifierList = null,
        array $additionalCriterionList = [],
        int $limit = 0,
        int $offset = 0,
        ?array $sortClauseList = null,
        bool $ignoreVisibility = false
    ): array {
        $query = $this->getDirectChildrenLocationQuery(
            $location,
            $contentTypeIdentifierList,
            $limit,
            $offset,
            $sortClauseList,
            $additionalCriterionList,
            $ignoreVisibility
        );

        try {
            return $this->fetchContentListFromSearchResult(
                $this->searchService->findLocations($query)
            );
        } catch (APIInvalidArgumentExceptionAlias $e) {
            throw new InvalidArgumentException('Bad request', $e->getCode(), $e);
        }
    }

    /**
     * @param Location             $location
     * @param null|string|string[] $contentTypeIdentifierList
     * @param Criterion[]          $additionalCriterionList
     * @param null|SortClause[]    $sortClauseList
     * @param bool                 $ignoreVisibility
     * @return Content|null
     */
    public function loadDirectChildrenSingleContent(
        Location $location,
        $contentTypeIdentifierList = null,
        array $additionalCriterionList = [],
        ?array $sortClauseList = null,
        bool $ignoreVisibility = false
    ): ?Content {
        $query = $this->getDirectChildrenLocationQuery(
            $location,
            $contentTypeIdentifierList,
            1,
            0,
            $sortClauseList,
            $additionalCriterionList,
            $ignoreVisibility
        );

        try {
            return $this->fetchSingleContentFromSearchResult(
                $this->searchService->findLocations($query)
            );
        } catch (APIInvalidArgumentExceptionAlias $e) {
            throw new InvalidArgumentException('Bad query', $e->getCode(), $e);
        }
    }

    /**
     * @param Location             $location
     * @param null|string|string[] $contentTypeIdentifierList
     * @param Criterion[]          $additionalCriterionList
     * @param null|SortClause[]    $sortClauseList
     * @return Pagerfanta<Content>
     */
    public function loadDirectChildrenContentPager(
        Location $location,
        $contentTypeIdentifierList = null,
        array $additionalCriterionList = [],
        ?array $sortClauseList = null
    ): Pagerfanta {
        $query = $this->getDirectChildrenLocationQuery(
            $location,
            $contentTypeIdentifierList,
            0,
            0,
            $sortClauseList,
            $additionalCriterionList
        );

        $adapter = new ContentSearchAdapter($query, $this->searchService);

        return new Pagerfanta($adapter);
    }

    /**
     * @param Location             $location
     * @param null|string|string[] $contentTypeIdentifierList
     * @param Criterion[]          $additionalCriterionList
     * @param null|SortClause[]    $sortClauseList
     * @return Pagerfanta<Location>
     */
    public function loadDirectChildrenLocationPager(
        Location $location,
        $contentTypeIdentifierList = null,
        array $additionalCriterionList = [],
        ?array $sortClauseList = null
    ): Pagerfanta {
        $query = $this->getDirectChildrenLocationQuery(
            $location,
            $contentTypeIdentifierList,
            0,
            0,
            $sortClauseList,
            $additionalCriterionList
        );

        $adapter = new LocationSearchAdapter($query, $this->searchService);

        return new Pagerfanta($adapter);
    }

    /**
     * @param Location             $location
     * @param null|string|string[] $contentTypeIdentifierList
     * @param Criterion[]          $additionalCriterionList
     * @param null|SortClause[]    $sortClauseList
     * @return Pagerfanta<Content>
     */
    public function loadAnyDepthChildrenContentPager(
        Location $location,
        $contentTypeIdentifierList = null,
        array $additionalCriterionList = [],
        ?array $sortClauseList = null
    ): Pagerfanta {
        $query = $this->getAnyDepthChildrenLocationQuery(
            $location,
            $contentTypeIdentifierList,
            0,
            0,
            $sortClauseList,
            $additionalCriterionList
        );

        return new Pagerfanta(new ContentSearchAdapter($query, $this->searchService));
    }

    /**
     * @param Location             $location
     * @param null|string|string[] $contentTypeIdentifierList
     * @param Criterion[]          $additionalCriterionList
     * @param null|SortClause[]    $sortClauseList
     * @return Pagerfanta<Location>
     */
    public function loadAnyDepthChildrenLocationPager(
        Location $location,
        $contentTypeIdentifierList = null,
        array $additionalCriterionList = [],
        ?array $sortClauseList = null
    ): Pagerfanta {
        $query = $this->getAnyDepthChildrenLocationQuery(
            $location,
            $contentTypeIdentifierList,
            0,
            0,
            $sortClauseList,
            $additionalCriterionList
        );

        return new Pagerfanta(new LocationSearchAdapter($query, $this->searchService));
    }

    /**
     * @param string       $contentTypeIdentifier
     * @param int          $limit
     * @param int          $offset
     * @param SortClause[] $sortClauseList
     * @param Criterion[]  $additionalCriterionList
     * @return Content[]
     */
    public function loadContentList(
        string $contentTypeIdentifier,
        int $limit = 0,
        int $offset = 0,
        array $sortClauseList = [],
        array $additionalCriterionList = []
    ): array {
        try {
            $locationQuery = $this->buildCommonLocationQuery(
                null,
                $contentTypeIdentifier,
                $limit,
                $offset,
                $sortClauseList,
                $additionalCriterionList
            );

            return $this->fetchContentListFromSearchResult(
                $this->searchService->findLocations($locationQuery)
            );
        } catch (APIInvalidArgumentExceptionAlias $e) {
            throw new InvalidArgumentException('Bad query', $e->getCode(), $e);
        }
    }

    /**
     * @param Location $location
     * @return SortClause[]
     */
    public function getSortClausesFromLocation(Location $location): array
    {
        $sortClause = null;

        if ($location->sortOrder === Location::SORT_ORDER_ASC) {
            $direction = Query::SORT_ASC;
        } else {
            $direction = Query::SORT_DESC;
        }

        switch ($location->sortField) {
            case Location::SORT_FIELD_CONTENTOBJECT_ID:
                $sortClause = new SortClause\ContentId($direction);
                break;
            case Location::SORT_FIELD_DEPTH:
                $sortClause = new SortClause\Location\Depth($direction);
                break;
            case Location::SORT_FIELD_MODIFIED:
                $sortClause = new SortClause\DateModified($direction);
                break;
            case Location::SORT_FIELD_NAME:
                $sortClause = new SortClause\ContentName($direction);
                break;
            case Location::SORT_FIELD_NODE_ID:
                $sortClause = new SortClause\Location\Id($direction);
                break;
            case Location::SORT_FIELD_PATH:
                $sortClause = new SortClause\Location\Path($direction);
                break;
            case Location::SORT_FIELD_SECTION:
                $sortClause = new SortClause\SectionName($direction);
                //qui aurait aussi pu être section_identifier, mais je trouvais ça plus pertinent
                break;
            case Location::SORT_FIELD_PUBLISHED:
                $sortClause = new SortClause\DatePublished($direction);
                break;
            case Location::SORT_FIELD_PRIORITY:
                $sortClause = new SortClause\Location\Priority($direction);
                break;
            case Location::SORT_FIELD_CLASS_IDENTIFIER:
            case Location::SORT_FIELD_CLASS_NAME:
                //À moins que je ne l'ai loupé, il s'agit d'ordre impossible avec une Query ?
                return [];
        }

        return $sortClause ? [$sortClause] : [];
    }

    /**
     * @param Location|null        $parentLocation
     * @param null|string|string[] $contentTypeIdentifierList
     * @param int                  $limit
     * @param int                  $offset
     * @param null|SortClause[]    $sortClauseList
     * @param Criterion[]          $criterionList
     * @param bool                 $ignoreVisibility
     * @return LocationQuery
     */
    public function buildCommonLocationQuery(
        ?Location $parentLocation,
        $contentTypeIdentifierList = null,
        int $limit = 0,
        int $offset = 0,
        ?array $sortClauseList = null,
        array $criterionList = [],
        bool $ignoreVisibility = false
    ): LocationQuery {
        $prioritizedLanguages = $this->languageResolver->getPrioritizedLanguages();
        if ($prioritizedLanguages) {
            $criterionList[] = new Criterion\LanguageCode($prioritizedLanguages);
        }

        if (!$ignoreVisibility) {
            $criterionList[] = new Criterion\Visibility(Criterion\Visibility::VISIBLE);
        }

        if (!empty($contentTypeIdentifierList)) {
            $criterionList[] = new Criterion\ContentTypeIdentifier($contentTypeIdentifierList);
        }

        $query = new LocationQuery();
        $query->query = new Criterion\LogicalAnd($criterionList);

        if ($parentLocation) {
            $query->sortClauses = $sortClauseList ?? $this->getSortClausesFromLocation($parentLocation);
        }

        $query->limit = $limit === 0 ? self::REASONABLY_HIGH_LIMIT : $limit;

        if ($offset) {
            $query->offset = $offset;
        }
        return $query;
    }

    /**
     * @param Location             $location
     * @param null|string|string[] $contentTypeIdentifierList
     * @param int                  $limit
     * @param int                  $offset
     * @param null|SortClause[]    $sortClauseList
     * @param Criterion[]          $additionalCriterionList
     * @param bool                 $ignoreVisibility
     * @return LocationQuery
     */
    public function getDirectChildrenLocationQuery(
        Location $location,
        $contentTypeIdentifierList = null,
        int $limit = 0,
        int $offset = 0,
        ?array $sortClauseList = null,
        array $additionalCriterionList = array(),
        bool $ignoreVisibility = false
    ): LocationQuery {
        $additionalCriterionList = array_merge(
            $additionalCriterionList,
            array(
                new Criterion\ParentLocationId($location->id),
            )
        );

        return $this->buildCommonLocationQuery(
            $location,
            $contentTypeIdentifierList,
            $limit,
            $offset,
            $sortClauseList,
            $additionalCriterionList,
            $ignoreVisibility
        );
    }

    /**
     * @param Location             $location
     * @param null|string|string[] $contentTypeIdentifierList
     * @param int                  $limit
     * @param int                  $offset
     * @param null|SortClause[]    $sortClauseList
     * @param Criterion[]          $additionalCriterionList
     * @param bool                 $ignoreVisibility
     * @return LocationQuery
     */
    public function getAnyDepthChildrenLocationQuery(
        Location $location,
        $contentTypeIdentifierList = null,
        int $limit = 0,
        int $offset = 0,
        ?array $sortClauseList = null,
        array $additionalCriterionList = array(),
        bool $ignoreVisibility = false
    ): LocationQuery {
        $additionalCriterionList = array_merge(
            $additionalCriterionList,
            array(
                new Criterion\Subtree($location->pathString),
            )
        );

        return $this->buildCommonLocationQuery(
            $location,
            $contentTypeIdentifierList,
            $limit,
            $offset,
            $sortClauseList,
            $additionalCriterionList,
            $ignoreVisibility
        );
    }

    /**
     * @param SearchResult<SearchHit> $searchResult
     * @return Location[]
     */
    public function fetchLocationListFromSearchResult(SearchResult $searchResult): array
    {
        return array_map(static function (SearchHit $searchHit): Location {
            $location = $searchHit->valueObject;
            if (!$location instanceof Location) {
                throw new DomainException('Unexpected location type');
            }

            return $location;
        }, $searchResult->searchHits);
    }

    /**
     * @param SearchResult<SearchHit> $searchResult
     * @return Content[]
     */
    public function fetchContentListFromSearchResult(SearchResult $searchResult): array
    {
        return array_map(static function (SearchHit $searchHit): Content {
            $location = $searchHit->valueObject;
            if (!$location instanceof Location) {
                throw new DomainException('Unexpected location type');
            }

            return $location->getContent();
        }, $searchResult->searchHits);
    }

    /**
     * @param SearchResult<SearchHit> $searchResult
     * @return Location|null
     */
    public function fetchSingleLocationFromSearchResult(SearchResult $searchResult): ?Location
    {
        $location = $searchResult->searchHits[0]->valueObject ?? null;
        if ($location instanceof Location) {
            return $location;
        }

        return null;
    }

    /**
     * @param SearchResult<SearchHit> $searchResult
     * @return Content|null
     */
    public function fetchSingleContentFromSearchResult(SearchResult $searchResult): ?Content
    {
        $location = $searchResult->searchHits[0]->valueObject ?? null;
        if ($location instanceof Location) {
            return $location->getContent();
        }

        return null;
    }
}
