<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentUtils\Traits;

use Ibexa\Contracts\Core\Repository\Values\Content\Location;
use Ibexa\Core\MVC\Symfony\View\ContentView;

/**
 * @deprecated Considered useless (light logic which have little to no impact on usage) : to me removed in 3.0
 */
trait GetLocationFromView
{
    protected function getLocationFromView(ContentView $view): ?Location
    {
        $location = $view->getLocation();

        return $location && $location->id ? $location : null;
    }
}
