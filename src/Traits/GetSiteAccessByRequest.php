<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentUtils\Traits;

use DomainException;
use Ibexa\Core\MVC\Symfony\SiteAccess;
use Symfony\Component\HttpFoundation\Request;

trait GetSiteAccessByRequest
{
    /**
     * Used to easily get the SiteAccess from a request (help with autocompletion and static analyzer)
     *
     * @param Request $request
     * @return SiteAccess
     */
    private function getSiteAccessByRequest(Request $request): SiteAccess
    {
        $siteAccess = $request->attributes->get('siteaccess');
        if (!$siteAccess instanceof SiteAccess) {
            throw new DomainException('Unable to get site access, got : ' . var_export($siteAccess, true));
        }

        return $siteAccess;
    }
}
