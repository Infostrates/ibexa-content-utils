<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentUtils\Traits;

use Ibexa\Contracts\Core\FieldType\Value;

trait IsFieldValueSeemsToBeByLanguage
{
    /**
     * Used to identify a field value structure which seems to be "by language"
     * (used in a migration, or RepositoryWriteHelper context)
     * @param array<string, string|Value|array<string, string|Value>>$fieldValueOrFieldValueByLanguage
     * @return bool
     */
    private function isFieldValueSeemsToBeByLanguage(array $fieldValueOrFieldValueByLanguage): bool
    {
        return (bool)preg_match(
            '/[a-z]{3}-[A-Z]{2}/',
            (string)(array_keys($fieldValueOrFieldValueByLanguage)[0] ?? '')
        );
    }
}
