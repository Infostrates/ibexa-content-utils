<?php

namespace Infostrates\IbexaContentUtils\Traits;

use Infostrates\IbexaContentUtils\RepositoryHelper;
use Ibexa\Contracts\Core\Repository\Values\Content\Location;

/**
* @deprecated Useless now that Ibexa PHP API's Location has direct access to parent : to me removed in 3.0
*/
trait GetParentLocation
{
    protected RepositoryHelper $repositoryHelper;

    /**
     * @param RepositoryHelper $repositoryHelper
     */
    public function __construct(RepositoryHelper $repositoryHelper)
    {
        $this->repositoryHelper = $repositoryHelper;
    }

    private function getParentLocation(?Location $location): ?Location
    {
        if (!$location) {
            return null;
        }

        return $this->repositoryHelper->loadLocation($location->parentLocationId);
    }
}
