<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentUtils\Traits;

/**
 * @deprecated Considered useless as this logic is a little to specific to be in a utility
 * bundle : to me removed in 3.0
 */

trait GetRandomElementList
{
    /**
     * @param array<int, mixed> $fullElementList
     * @param int               $desiredElementCount
     * @return array<int, mixed>
     */
    private function getRandomElementList(array $fullElementList, int $desiredElementCount): array
    {
        if (count($fullElementList) <= $desiredElementCount) {
            $randomElementList = $fullElementList;
        } else {
            $randomlyPickedArrayKey = (array)array_rand($fullElementList, $desiredElementCount);
            $randomElementList = [];
            foreach ($randomlyPickedArrayKey as $key) {
                $randomElementList[] = $fullElementList[$key];
            }
        }

        shuffle($randomElementList);

        return $randomElementList;
    }
}
