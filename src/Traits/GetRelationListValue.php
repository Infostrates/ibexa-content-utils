<?php

namespace Infostrates\IbexaContentUtils\Traits;

use DomainException;
use Ibexa\Contracts\Core\Repository\Values\Content\Content;
use Ibexa\Core\FieldType;
use Ibexa\Core\Helper\TranslationHelper;

trait GetRelationListValue
{
    /**
     * Used to easily get a ezobjectrelationlist Value, to put in
     * Infostrates\IbexaContentUtils\FieldHelper::loadContentListFromRelationListValue
     */
    private function getRelationListValue(Content $content, string $fieldIdentifier): FieldType\RelationList\Value
    {
        $fieldValue = $content->getFieldValue($fieldIdentifier);

        if (!$fieldValue instanceof FieldType\RelationList\Value) {
            throw new DomainException(sprintf(
                'Field %s should be of type RelationList/Value got %s',
                $fieldIdentifier,
                $fieldValue ? get_class($fieldValue) : 'null'
            ));
        }
        return $fieldValue;
    }
}
