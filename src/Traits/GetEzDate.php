<?php

namespace Infostrates\IbexaContentUtils\Traits;

use DateTime;
use DateTimeInterface;
use DateTimeZone;
use LogicException;

trait GetEzDate
{
    /**
     * Transform a date to an UTC date which can be used to compare
     * \Ibexa\Core\FieldType\Date\Value or \Ibexa\Core\FieldType\DateTime\Value values, or query Ibexa's API
     *
     * @phpstan-template T of DateTimeInterface
     * @phpstan-param T $dateFieldDateTime
     * @phpstan-return T
     */
    private function getEzDate(DateTimeInterface $dateFieldDateTime): DateTimeInterface
    {
        $dateTimeClass = get_class($dateFieldDateTime);
        $realDate = $dateTimeClass::createFromFormat(
            '!Y-m-d',
            $dateFieldDateTime->format('Y-m-d'),
            new DateTimeZone('UTC')
        );
        if (!$realDate) {
            throw new LogicException('Something went terribly wrong');
        }

        return $realDate;
    }
}
