<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentUtils;

use DomainException;
use Ibexa\Contracts\Core\Repository\Values\ContentType\ContentType;
use Ibexa\Contracts\Core\SiteAccess\ConfigResolverInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

use function in_array;

class PageContentTypeGuesser
{
    private ConfigResolverInterface $configResolver;
    /**
     * @var string[]
     */
    private array $otherPageContentList;

    /**
     * @param ConfigResolverInterface $configResolver
     * @param string[] $otherPageContentList
     */
    public function __construct(ConfigResolverInterface $configResolver, array $otherPageContentList)
    {
        $this->configResolver = $configResolver;
        $this->otherPageContentList = $otherPageContentList;
    }

    /**
     * @param ContentType $contentType
     * @param string|null $targetSiteAccessName If left null, will be the current site access
     * @return bool Return true if this content of this content type has its own page
     */
    /**
     * @param ContentType $contentType
     * @param string|null $targetSiteAccessName If left null, will be the current site access
     * @return bool Return true if this content of this content type has its own page
     */
    public function hasContentTypeHaveContentPage(ContentType $contentType, ?string $targetSiteAccessName = null): bool
    {
        $allFullContentTypeIdentifierList = $this->getAllFullContentTypeIdentifierList($targetSiteAccessName);
        $allOtherContentTypeIdentifierList = $this->getAllOtherContentTypeIdentifierList($targetSiteAccessName);
        if (!empty($allOtherContentTypeIdentifierList)) {
            $allFullContentTypeIdentifierList = array_merge(
                $allFullContentTypeIdentifierList,
                $allOtherContentTypeIdentifierList
            );
        }

        return in_array($contentType->identifier, $allFullContentTypeIdentifierList, true);
    }

    /**
     * @param string|null $targetSiteAccessName If left null, will be the current site access
     * @return string[]
     */
    public function getAllFullContentTypeIdentifierList(?string $targetSiteAccessName = null): array
    {
        static $allFullContentTypeIdentifierList;

        if ($allFullContentTypeIdentifierList === null) {
            $allFullContentTypeIdentifierList = $this->getContentTypeIdentifierList(
                'full',
                $targetSiteAccessName
            );
        }

        return $allFullContentTypeIdentifierList;
    }

    /**
     * @param string|null $targetSiteAccessName If left null, will be the current site access
     * @return string[]|null
     */
    public function getAllOtherContentTypeIdentifierList(?string $targetSiteAccessName = null): ?array
    {
        static $allOtherContentTypeIdentifierList;

        if ($allOtherContentTypeIdentifierList === null && !empty($this->otherPageContentList)) {
            $listContentTypeIdentifier = [];
            foreach ($this->otherPageContentList as $otherPageContent) {
                $listContentTypeIdentifier[] = $this->getContentTypeIdentifierList(
                    $otherPageContent,
                    $targetSiteAccessName
                );
            }

            $allOtherContentTypeIdentifierList = array_merge([], ...$listContentTypeIdentifier);
        }

        return $allOtherContentTypeIdentifierList;
    }

    /**
     * @param string $domain
     * @param string|null $targetSiteAccessName
     * @return string[]
     */
    public function getContentTypeIdentifierList(string $domain, ?string $targetSiteAccessName): array
    {
        $contentViewList = $this->configResolver->getParameter(
            'content_view',
            null,
            $targetSiteAccessName
        )[$domain] ?? null;

        if (!$contentViewList) {
            throw new DomainException('No ' . $domain . ' view detected');
        }

        $listContentTypeIdentifier = [];
        foreach ($contentViewList as $contentView) {
            $matchList = $contentView['match'] ?? [];
            if (!is_array($matchList)) {
                throw new DomainException('Bad match structure');
            }

            foreach ($matchList as $matchType => $matchValue) {
                if ($matchType === 'Identifier\ContentType') {
                    $listContentTypeIdentifier[] = (array)$matchValue;
                }
            }
        }
        return array_merge([], ...$listContentTypeIdentifier);
    }
}
