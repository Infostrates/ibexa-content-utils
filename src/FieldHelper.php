<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentUtils;

use DomainException;
use Ibexa\Contracts\Core\Repository\ContentService;
use Ibexa\Contracts\Core\Repository\ContentTypeService;
use Ibexa\Contracts\Core\Repository\Exceptions\InvalidArgumentException as APIInvalidArgumentExceptionAlias;
use Ibexa\Contracts\Core\Repository\Exceptions\NotFoundException as ApiNotFoundException;
use Ibexa\Contracts\Core\Repository\Exceptions\UnauthorizedException as ApiUnauthorizedException;
use Ibexa\Contracts\Core\Repository\SearchService;
use Ibexa\Contracts\Core\Repository\Values\Content\Content;
use Ibexa\Core\FieldType;
use Ibexa\Core\Helper\FieldHelper as BaseFieldHelper;
use Ibexa\Contracts\Core\Repository\Values\Content\Query\Criterion;
use InvalidArgumentException;

class FieldHelper
{
    private BaseFieldHelper $fieldHelper;
    private ContentTypeService $contentTypeService;
    private SearchServiceHelper $searchServiceHelper;
    private SearchService $searchService;
    private ContentService $contentService;

    /**
     * @param BaseFieldHelper     $fieldHelper
     * @param ContentTypeService  $contentTypeService
     * @param SearchServiceHelper $searchServiceHelper
     * @param SearchService       $searchService
     * @param ContentService      $contentService
     */
    public function __construct(
        BaseFieldHelper $fieldHelper,
        ContentTypeService $contentTypeService,
        SearchServiceHelper $searchServiceHelper,
        SearchService $searchService,
        ContentService $contentService
    ) {
        $this->fieldHelper = $fieldHelper;
        $this->contentTypeService = $contentTypeService;
        $this->searchServiceHelper = $searchServiceHelper;
        $this->searchService = $searchService;
        $this->contentService = $contentService;
    }

    /**
     * Checks if provided field can be considered empty.
     */
    public function isFieldEmpty(Content $content, string $fieldDefIdentifier, ?string $forcedLanguage = null): bool
    {
        return $this->fieldHelper->isFieldEmpty($content, $fieldDefIdentifier, $forcedLanguage);
    }

    /**
     * @param string $contentTypeIdentifier
     * @param string $selectionFieldIdentifier
     * @return array<string, string> de la forme ['id' => 'label', 'id2' => 'label2', ...]
     * @throws ApiNotFoundException
     */
    public function getSelectionLabelMapping(string $contentTypeIdentifier, string $selectionFieldIdentifier): array
    {
        $fieldDefinition = $this->contentTypeService
            ->loadContentTypeByIdentifier($contentTypeIdentifier)
            ->getFieldDefinition($selectionFieldIdentifier);
        if (!$fieldDefinition) {
            throw new DomainException(
                sprintf(
                    'Selection field %s does not exist for content type %s',
                    $selectionFieldIdentifier,
                    $contentTypeIdentifier
                )
            );
        }

        return $fieldDefinition->fieldSettings['options'];
    }

    /**
     * @param FieldType\RelationList\Value $relationListValue
     * @return Content[]
     */
    public function loadContentListFromRelationListValue(FieldType\RelationList\Value $relationListValue): array
    {
        /** @var int[] $destinationContentIds */
        $destinationContentIds = $relationListValue->destinationContentIds;
        if (empty($destinationContentIds)) {
            return [];
        }

        $locationQuery = $this->searchServiceHelper->buildCommonLocationQuery(
            null,
            null,
            0,
            0,
            null,
            [
                new Criterion\ContentId($destinationContentIds),
            ]
        );

        try {
            $searchResult = $this->searchService->findLocations($locationQuery);

            $contentList = $this->searchServiceHelper->fetchContentListFromSearchResult($searchResult);

            $contentIdMet = [];
            $uniqueContentList = array_filter($contentList, static function (Content $content) use (&$contentIdMet) {
                if (in_array($content->id, $contentIdMet, true)) {
                    return false;
                }

                $contentIdMet[] = $content->id;
                return true;
            });

            $rankByContentId = array_flip($destinationContentIds);
            usort(
                $uniqueContentList,
                static function (Content $contentA, Content $contentB) use ($rankByContentId) {
                    $rankA = $rankByContentId[$contentA->id] ?? null;
                    $rankB = $rankByContentId[$contentB->id] ?? null;
                    return $rankA <=> $rankB;
                }
            );

            return $uniqueContentList;
        } catch (APIInvalidArgumentExceptionAlias $e) {
            throw new InvalidArgumentException('Bad query', $e->getCode(), $e);
        }
    }

    /**
     * @param FieldType\RelationList\Value $relationListValue
     * @return Content
     */
    public function loadSingleContentFromRelationListValue(FieldType\RelationList\Value $relationListValue): ?Content
    {
        // Its intended to load the first content available : do not remove this iteration
        foreach ($relationListValue->destinationContentIds as $contentId) {
            try {
                return $this->contentService->loadContent($contentId);
            } catch (ApiNotFoundException | ApiUnauthorizedException $e) {
                continue;
            }
        }

        return null;
    }
}
