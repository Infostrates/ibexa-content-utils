Infostrates Ibexa Content utils
===============================

A collection of helper components to handle general problematics on an Ibexa Content (or Ibexa OSS) project.

Branch 2.0 is for a Bootstrap front
Branches 1.x is for a Foundation front

# Installation

Make sure Composer is installed globally, as explained in the
[installation chapter](https://getcomposer.org/doc/00-intro.md)
of the Composer documentation.

### Step 1: Download the Bundle

Open a command console, enter your project directory and execute the
following command to download the latest stable version of this bundle:

```console
$ composer config repositories.infostrates_ibexa_content_utils vcs https://bitbucket.org/Infostrates/ibexa-content-utils.git
$ composer require infostrates/ibexa-content-utils
```

# Components

## Image

Provide twig functions to easily render an img tag from an image field :

### ez_src()

Output the string src value of a given content image, in the given image variation. Useful for a background for example.

Usage: ```ez_src(<content>, <field identifier>, <image variation name>)```

Where :

- ```<content>``` is a \Ibexa\Contracts\Core\Repository\Values\Content\Content object
- ```<field identifier>``` is the identifier of the image field to get src
- ```<image variation name>``` a previously set image variation name

### responsive_image()

Return an \Infostrates\IbexaContentUtils\Image\TwigExtension\ResponsiveImage to give to the following ez_render_img_tag
function to get a responsive image.

Usage: ```responsive_image(<image variation name list>, <chosen "sizes" attribute value>)```

Where :

- ```<image variation name list>``` is a list of previously set image variation name
- ```<chosen "sizes" attribute value>``` is the value of the target img's sizes attribute.

See ez_render_img_tag below for example.

### ez_render_img_tag()

Render an img tag from a content image, with the given parameters.
Usage: ```ez_render_img_tag(<content>, <field identifier>, <image variation name or ResponsiveImage>, <custom html attribute for the img tag>?, <add width and height attributes to img tag>?, <use content name as default alt>?)```

Where:

- ```<content>``` is a \Ibexa\Contracts\Core\Repository\Values\Content\Content object
- ```<field identifier>``` is the identifier of the image field to get src
- ```<image variation name or ResponsiveImage>``` a previously set image variation name, or a ResponsiveImage (see
  responsive_image() above)
- Optional ```<custom html attribute for the img tag>``` (default: empty) allow to add any html attribute to the img tag
- Optional ```<add width and height attributes to img tag>``` (default: false) automatically add width and height based
  on the image variation (or the first image variation of a ResponsiveImage)
- Optional ```<use content name as default alt>``` (default: false) automatically fill empty alt (set in admin UI) by
  the content name

Simple example : ```{{ ez_render_img_tag(content, 'image', 'original') }}``` will
render: ```<img alt="" loading="lazy" src="http://localhost:8042/var/site/storage/images/3/5/2/0/253-1-eng-GB/e2df6d9fe342-4.jpg">```

Example with responsive
image : ```{{ ez_render_img_tag(content, 'hero_image', responsive_image(['original', 'large'], '100vw'), {'class': 'qa-image page-title__image', 'loading': null}) }}```
will
render ```<img alt="" srcset="http://localhost:8042/var/site/storage/images/9/3/2/0/239-1-eng-GB/d587aa6a732b-1.jpg 1080w,http://localhost:8042/var/site/storage/images/_aliases/large/9/3/2/0/239-1-eng-GB/d587aa6a732b-1.jpg 800w" sizes="100vw" src="http://localhost:8042/var/site/storage/images/9/3/2/0/239-1-eng-GB/d587aa6a732b-1.jpg" class="qa-image page-title__image">```

Example with responsive (include sizes with media-queries) and sizes attr
image : ```{{ ez_render_img_tag(content, 'hero_image', responsive_image(['original', 'large'], [['75vw', '(max-width: 1024px)'], '100vw']), {'class': 'qa-image page-title__image', 'loading': null}, true) }}```
will
render ```<img alt="" srcset="http://localhost:8042/var/site/storage/images/9/3/2/0/239-1-eng-GB/d587aa6a732b-1.jpg 1080w,http://localhost:8042/var/site/storage/images/_aliases/large/9/3/2/0/239-1-eng-GB/d587aa6a732b-1.jpg 800w" sizes="100vw" src="http://localhost:8042/var/site/storage/images/9/3/2/0/239-1-eng-GB/d587aa6a732b-1.jpg" class="qa-image page-title__image">```

__Note for ResponsiveImage:__ this function will automatically set srcset "w" based on each image variation actual sizes
of the image, not the target size of the image variation. This means that small image may generate a non-pertinent
srcset.

Here's a full blog post about srcset and sizes : https://ericportis.com/posts/2014/srcset-sizes/
Attributes documentation:
https://developer.mozilla.org/en-US/docs/Web/HTML/Element/img#attr-srcset
https://developer.mozilla.org/en-US/docs/Web/HTML/Element/img#attr-sizes

## InternalOrExternalLink

Provide an easy internal / external link system for content.

Ibexa does not offer an "all-in-one" field type for link. You have to choice between internal link (based on
ezobjectrelationlist) or external link (based on ezurl, which as an additional field for a button, which is not always
pertinent).

This component helps to cover this problem, by using 3 fields with a naming convention (all are optional, depending on
your context) :

- internal_link (type : ezobjectrelationlist): will generate a link to the selected content main location
- internal_link_complement (type: ezstring) : will be appended to the url generated by the previous field. It will allow
  you to add anchor or query string to your internal urls.
- external_link (type: ezstring) if you want to render an external link, simply render the inputted url

Note that you can also prefix them to have multiple internal / external link by content (example, prefixed by block1 :
block1_internal_link, block1_internal_link_complement, block1_external_link)

The component now offer you 2 twig functions :

### ez_internal_or_external_link()

Return a nullable \Infostrates\IbexaContentUtils\InternalOrExternalLink\Model\Link object that can be rendered by
link_attr below.

External link will be automatically have their Link target set to "_blank"

Usage: ```ez_internal_or_external_link(<content>, <prefix field identifier>?)```

Where:

- ```<content>``` is a \Ibexa\Contracts\Core\Repository\Values\Content\Content object
- Optional ```<prefix field identifier>``` to get a prefixed set of field (see above)

### link_attr()

Render attributes to be put in a ```<a>``` html element.

Usage: ```link_attr(<Nullable Link object>, <force target>?)```

Where:

- ```<Nullable Link object>``` is a \Infostrates\IbexaContentUtils\InternalOrExternalLink\Model\Link object, or null
- Optional ```<force target>``` to set a target manually. If not set, it will use the one set in the Link (which will
  be "_blank" for external)

Example in twig : ```<a {{ link_attr(link) }}>{{ label }}</a>```

## Traits

A collection of traits for common operations, that you can find here with their documentation : [Traits](src/Traits)

## PageContentTypeGuesser

An utility to help you identify content type used as page based on content_view full configuration :
Infostrates\IbexaContentUtils\PageContentTypeGuesser ([PageContentTypeGuesser.php](src/PageContentTypeGuesser.php))

## RepositoryHelpers

A collection of helper to ease Ibexa's PHP API usage.

- Infostrates\IbexaContentUtils\ContentServiceHelper ([ContentServiceHelper.php](src/ContentServiceHelper.php)) : Same
  loadContent and loadContentBy than Ibexa\Contracts\Core\Repository\ContentService, but with nullable return and handling of
  language and visibility.
- Infostrates\IbexaContentUtils\LocationServiceHelper ([LocationServiceHelper.php](src/LocationServiceHelper.php)) :
  Same loadLocation and loadLocationBy than Ibexa\Contracts\Core\Repository\LocationService, but with nullable return and
  handling of language and visibility.
- Infostrates\IbexaContentUtils\FieldHelper ([FieldHelper.php](src/FieldHelper.php)) : Same vein than
  \Ibexa\Core\Helper\FieldHelper, adding utility methods to field related problematics.
- Infostrates\IbexaContentUtils\SearchServiceHelper ([SearchServiceHelper.php](src/SearchServiceHelper.php)) : Lots of
  search shortcut to get Content or Location list, or even Pager. All of them setting appropriate query for language,
  visibility and "by location" configured default sort.

## RepositoryWriterHelper

Helper to make basic write operation (create or update content) :
Infostrates\IbexaContentUtils\RepositoryWriteHelper [RepositoryWriteHelper.php](src/RepositoryWriteHelper.php)

Note that field values must be simple scalar, or Ibexa\Core\FieldType\Value, but there's no smart value handling
like in ezmigrationbundle2. However, a POC was made in VB App\Tests\Domains\Destination\DefaultContent\Provider::
getComplexFieldValues(), which might be easily implemented in this bundle ;)

## Wysiwyg

2 twig filters to handle common problematics with RichText field type outputted html :

- Infostrates\IbexaContentUtils\Wysiwyg\LimitHLevelExtension ([LimitHLevelExtension.php](src/Wysiwyg/LimitHLevelExtension.php)) :
Used to set a minimum level for title. |limit_h_level(2) will render richtext ```<h1>``` as ```<h2>```, ```<h2>``` as ```<h3>```, etc... Note
that there's a better implementation with an optional class here in VB's
App\Infrastructure\Wysiwyg\TwigExtensions\EzRichtextToHtml5Improved (in a simpler form in the boilerplate, which render
this helper useless).
- Infostrates\IbexaContentUtils\Wysiwyg\ResponsiveIframeExtension ([ResponsiveIframeExtension.php](src/Wysiwyg/ResponsiveIframeExtension.php)) :
Add a wrapper div with a class "responsive-embed widescreen", around all iframes

## Contributing to this bundle

No dev stack for now, you'll have to install php on your workspace. CI tests can be run locally with :

```
make test
```

Versions are made simply by tagging the desired commit (don't forget to push the tags !).

## TODO

- Récupérer l'ImageManipulation "Gravity center" du boilerplate
- Récupérer "App\Infrastructure\Utils\TemporarilyOpenRequiredField" du boilerplate
- Implémenter les tests d'injection en prenant exemple
  sur https://github.com/Codein-Labs/ibexa-seo-toolkit/blob/master/tests/bundle/DependencyInjection/ExtensionTest.php (
  php composer.phar require --dev matthiasnoback/symfony-service-definition-validator)
