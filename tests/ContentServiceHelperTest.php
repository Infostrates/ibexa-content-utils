<?php

namespace Infostrates\IbexaContentUtils\Tests;

use Ibexa\Contracts\Core\Repository\ContentService;
use Ibexa\Contracts\Core\Repository\LanguageResolver;
use Ibexa\Core\Base\Exceptions\NotFoundException;
use Ibexa\Core\Base\Exceptions\UnauthorizedException;
use Infostrates\IbexaContentUtils\ContentServiceHelper;
use Infostrates\IbexaContentUtils\Tests\Helpers\EzContentStub;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class ContentServiceHelperTest extends TestCase
{
    private ContentServiceHelper $testSubject;
    /** @var ContentService&MockObject */
    private ContentService $contentService;

    protected function setUp(): void
    {
        /** @var ContentService&MockObject $contentService */
        $contentService = $this->createMock(ContentService::class);
        $this->contentService = $contentService;

        /** @var LanguageResolver&MockObject $languageResolver */
        $languageResolver = $this->createMock(LanguageResolver::class);
        $languageResolver->method('getPrioritizedLanguages')->willReturn(['fre-FR']);

        $this->testSubject = new ContentServiceHelper($this->contentService, $languageResolver);
    }

    public function testLoadContent(): void
    {
        $fakeContent = EzContentStub::getTestFakeContent();
        $this->contentService->method('loadContent')->willReturn($fakeContent);

        $this->assertSame(
            $fakeContent,
            $this->testSubject->loadContent(42)
        );
    }

    public function testLoadContentNotFound(): void
    {
        $exception = new NotFoundException('content', 42);
        $this->contentService->method('loadContent')->willThrowException($exception);

        $this->assertNull(
            $this->testSubject->loadContent(42)
        );
    }

    public function testLoadContentUnauthorized(): void
    {
        $exception = new UnauthorizedException('content', 'read');
        $this->contentService->method('loadContent')->willThrowException($exception);

        $this->assertNull(
            $this->testSubject->loadContent(42)
        );
    }

    public function testLoadContentByRemoteId(): void
    {
        $fakeContent = EzContentStub::getTestFakeContent();
        $this->contentService->method('loadContentByRemoteId')->willReturn($fakeContent);

        $this->assertSame(
            $fakeContent,
            $this->testSubject->loadContentByRemoteId('remote-42')
        );
    }

    public function testLoadContentByRemoteIdNotFound(): void
    {
        $exception = new NotFoundException('content', 'remote-42');
        $this->contentService->method('loadContentByRemoteId')->willThrowException($exception);

        $this->assertNull(
            $this->testSubject->loadContentByRemoteId('remote-42')
        );
    }

    public function testLoadContentByRemoteIdUnauthorized(): void
    {
        $exception = new UnauthorizedException('content', 'read');
        $this->contentService->method('loadContentByRemoteId')->willThrowException($exception);

        $this->assertNull(
            $this->testSubject->loadContentByRemoteId('remote-42')
        );
    }

    public function testGetContentIdsFromContentList(): void
    {
        $this->assertSame(
            [42],
            $this->testSubject->getContentIdsFromContentList([
                EzContentStub::getTestFakeContent()
            ])
        );

        $this->assertSame(
            [42, 1337, 2000],
            $this->testSubject->getContentIdsFromContentList([
                EzContentStub::getTestFakeContent(),
                EzContentStub::getTestFakeContent(1337),
                EzContentStub::getTestFakeContent(2000),
            ])
        );

        $this->assertSame(
            [],
            $this->testSubject->getContentIdsFromContentList([])
        );
    }
}
