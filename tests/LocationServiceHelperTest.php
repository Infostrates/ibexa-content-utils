<?php

namespace Infostrates\IbexaContentUtils\Tests;

use Ibexa\Contracts\Core\Repository\LocationService;
use Ibexa\Contracts\Core\Repository\LanguageResolver;
use Ibexa\Core\Base\Exceptions\NotFoundException;
use Ibexa\Core\Base\Exceptions\UnauthorizedException;
use Ibexa\Contracts\Core\SiteAccess\ConfigResolverInterface;
use Infostrates\IbexaContentUtils\LocationServiceHelper;
use Infostrates\IbexaContentUtils\Tests\Helpers\EzContentStub;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class LocationServiceHelperTest extends TestCase
{
    private LocationServiceHelper $testSubject;
    /** @var LocationService&MockObject */
    private LocationService $locationService;

    protected function setUp(): void
    {
        /** @var LocationService&MockObject $locationService */
        $locationService = $this->createMock(LocationService::class);
        $this->locationService = $locationService;

        /** @var LanguageResolver&MockObject $languageResolver */
        $languageResolver = $this->createMock(LanguageResolver::class);
        $languageResolver->method('getPrioritizedLanguages')->willReturn(['fre-FR']);

        /** @var ConfigResolverInterface&MockObject $configResolver */
        $configResolver = $this->createMock(ConfigResolverInterface::class);
        $configResolver->method('getParameter')->willReturn(2);

        $this->testSubject = new LocationServiceHelper($this->locationService, $languageResolver, $configResolver);
    }

    public function testLoadLocation(): void
    {
        $fakeLocation = EzContentStub::getTestFakeLocation();
        $this->locationService->method('loadLocation')->willReturn($fakeLocation);

        $this->assertSame(
            $fakeLocation,
            $this->testSubject->loadLocation(51)
        );
    }

    public function testLoadLocationNotFound(): void
    {
        $exception = new NotFoundException('location', 51);
        $this->locationService->method('loadLocation')->willThrowException($exception);

        $this->assertNull(
            $this->testSubject->loadLocation(51)
        );
    }

    public function testLoadLocationUnauthorized(): void
    {
        $exception = new UnauthorizedException('location', 'read');
        $this->locationService->method('loadLocation')->willThrowException($exception);

        $this->assertNull(
            $this->testSubject->loadLocation(51)
        );
    }

    public function testLoadLocationByRemoteId(): void
    {
        $fakeLocation = EzContentStub::getTestFakeLocation();
        $this->locationService->method('loadLocationByRemoteId')->willReturn($fakeLocation);

        $this->assertSame(
            $fakeLocation,
            $this->testSubject->loadLocationByRemoteId('remote-51')
        );
    }

    public function testLoadLocationByRemoteIdNotFound(): void
    {
        $exception = new NotFoundException('location', 'remote-51');
        $this->locationService->method('loadLocationByRemoteId')->willThrowException($exception);

        $this->assertNull(
            $this->testSubject->loadLocationByRemoteId('remote-51')
        );
    }

    public function testLoadLocationByRemoteIdUnauthorized(): void
    {
        $exception = new UnauthorizedException('location', 'read');
        $this->locationService->method('loadLocationByRemoteId')->willThrowException($exception);

        $this->assertNull(
            $this->testSubject->loadLocationByRemoteId('remote-51')
        );
    }

    public function testGetLocationIdsFromLocationList(): void
    {
        $this->assertSame(
            [51],
            $this->testSubject->getLocationIdsFromLocationList([
                EzContentStub::getTestFakeLocation()
            ])
        );

        $this->assertSame(
            [51, 1337, 2000],
            $this->testSubject->getLocationIdsFromLocationList([
                EzContentStub::getTestFakeLocation(),
                EzContentStub::getTestFakeLocation(1337),
                EzContentStub::getTestFakeLocation(2000),
            ])
        );

        $this->assertSame(
            [],
            $this->testSubject->getLocationIdsFromLocationList([])
        );
    }

    public function testGetRootLocationId(): void
    {
        $this->assertSame($this->testSubject->getRootLocationId(), 2);
    }

    public function testLoadRootLocation(): void
    {
        $fakeLocation = EzContentStub::getTestFakeLocation();
        $this->locationService->method('loadLocation')->willReturn($fakeLocation);

        $this->assertSame($fakeLocation, $this->testSubject->loadRootLocation());
    }
}
