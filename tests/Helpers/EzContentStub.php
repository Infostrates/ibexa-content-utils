<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentUtils\Tests\Helpers;

use Ibexa\Contracts\Core\Repository\Values\Content\ContentInfo;
use Ibexa\Core\Repository\Values\Content\Content;
use Ibexa\Core\Repository\Values\Content\Location;
use Ibexa\Core\Repository\Values\Content\VersionInfo;
use Ibexa\Core\Repository\Values\ContentType\ContentType;

final class EzContentStub
{
    public static function getTestFakeContent(int $id = 42, ?string $contentTypeIdentifier = null): Content
    {
        $contentInfo = self::getTestFakeContentInfo($id);
        $versionInfo = new VersionInfo(['contentInfo' => $contentInfo, 'languageCodes' => ['fre-FR', 'eng-GB']]);

        $contentType = null;
        if ($contentTypeIdentifier) {
            $contentType = new ContentType(['identifier' => $contentTypeIdentifier]);
        }

        return new Content(['versionInfo' => $versionInfo, 'contentType' => $contentType]);
    }

    public static function getTestFakeLocation(?int $locationId = 51): Location
    {
        return new Location([
            'id' => $locationId,
            'contentInfo' => self::getTestFakeContentInfo(42),
        ]);
    }

    private static function getTestFakeContentInfo(int $id): ContentInfo
    {
        return new ContentInfo(['id' => $id, 'mainLanguageCode' => 'fre-FR']);
    }
}
